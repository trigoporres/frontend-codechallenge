import { mount } from '@vue/test-utils'
import ListDo from '@/components/listDo'

describe('Component LisDo', () => {
  const wrapper = mount(ListDo)

  it('should set tareas', () => {
    expect(wrapper.text('.tareas'))
  })
  it('should set search', () => {
    expect(wrapper.find('.search'))
  })
})
