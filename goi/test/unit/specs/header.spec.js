import { mount } from '@vue/test-utils'
import Header from '@/Header'

describe('Component LisDo', () => {
  let components

  it('should set tareas', () => {
    const wrapper = mount(Header, {
      stubs: components })
    expect(wrapper.text('.name')).toBe('Readme Santiago Trigo Porres')
  })
})
