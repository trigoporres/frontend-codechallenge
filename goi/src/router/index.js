import Vue from 'vue'
import Router from 'vue-router'
import ListDo from '@/components/listDo'
import Readme from '@/components/readme'

Vue.use(Router)

export default new Router({
  // cuando el usuario accede a la raiz se renderizara el componente LisDo y cuando haga click en el boton readme renderizara el componente Readme. Ambos componentes deben de ser importados para poder hacer uso de ellos
  routes: [
    {
      path: '/',
      name: 'ListDo',
      component: ListDo
    },
    {
      path: '/readme',
      name: 'readme',
      component: Readme
    }
  ]
})
